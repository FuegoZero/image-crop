const path = require('path')
const fs = require('fs')
const execSync = require('child_process').execSync

export function isImage (file) {
  return ['.BMP', '.JPG', '.JPEG', '.PNG', '.GIF'].includes(path.extname(file).toUpperCase())
}

export function checkGmInstalled () {
  try {
    execSync('gm version')
  } catch (err) {
    return false
  }
  return true
}

export function isDirectory (path) {
  return fs.statSync(path).isDirectory()
}

export function mkDirByPathSync (targetDir, { isRelativeToScript = false } = {}) {
  const sep = path.sep
  const initDir = path.isAbsolute(targetDir) ? sep : ''
  const baseDir = isRelativeToScript ? __dirname : '.'

  return targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(baseDir, parentDir, childDir)
    try {
      fs.mkdirSync(curDir)
    } catch (err) {
      if (err.code === 'EEXIST') { // curDir already exists!
        return curDir
      }

      // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
      if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
        throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`)
      }

      const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1
      if ((!caughtErr || caughtErr) && curDir === path.resolve(targetDir)) {
        throw err // Throw if it's just the last created dir.
      }
    }

    return curDir
  }, initDir)
}

export { path, fs }
