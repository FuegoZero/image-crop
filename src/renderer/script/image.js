const path = require('path')

class Image {
  constructor (rootPath, path, file) {
    this.rootPath = rootPath
    this.directory = path
    this.name = file
  }

  getFilePath () {
    return path.join(this.directory, this.name)
  }

  getDestPath (outputPath) {
    return path.join(this.getDestDirectory(outputPath), this.name)
  }

  getDestDirectory (outputPath) {
    return path.join(outputPath, path.relative(this.rootPath, this.directory))
  }
}

export {
  Image
}
