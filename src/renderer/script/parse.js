import { isImage, path, fs, isDirectory, mkDirByPathSync } from './utils'
import { Image } from './image'

const gm = require('gm')

function getImages (inputPath, outputPath) {
  let images = []
  let rootPath = inputPath

  ;(function check (inputPath) {
    fs.readdirSync(inputPath).map((x) => {
      if (isImage(x)) {
        return images.push(new Image(rootPath, inputPath, x))
      }

      let temp = path.join(inputPath, x)
      if (isDirectory(temp) && temp !== outputPath) return check(temp)
    })
  })(inputPath)

  return images
}

export default function ({ inputPath, outputPath, update, fail, success, fillColor = '#ffffff', targetWidth, targetHeight, mode }) {
  console.clear()

  let images = getImages(inputPath, outputPath)

  let total = images.length

  if (!fs.existsSync(outputPath)) mkDirByPathSync(outputPath, { recursive: true })

  function getSize (imgObj, img) {
    return new Promise((resolve, reject) => {
      imgObj.size((err, size) => {
        if (err)reject(fail(`解析图片【${img}】出现错误：${JSON.stringify(err)}`))
        let { height, width } = size
        resolve({ height, width })
      })
    })
  }

  function calc (primary, secondary) {
    if (primary >= 750) return Math.round((1 - (primary - 750) / primary) * secondary)
    if (primary <= 750) return Math.round((1 + (750 - primary) / primary) * secondary)
  }

  ;(async function check () {
    let img = images.shift()
    if (!img) return

    console.log(img)

    console.log('%c┏ ----------------------------', 'background-color:yellow')
    console.log(`开始解析图片：${img.getFilePath()}`)
    update(img.name, +((total - images.length) / total * 100).toFixed(0))
    let filePath = img.getFilePath()
    let imgObj = gm(filePath)

    let { width, height } = await getSize(imgObj, img.name)

    if (height < targetWidth && width < targetHeight) {
      let bgPath = path.join(outputPath, `._bg_${img.name}`)

      await new Promise((resolve, reject) => {
        gm(targetWidth, targetHeight, fillColor).write(bgPath, (err) => {
          if (err) return null
          imgObj = gm()
            .command('composite')
            .in('-gravity', 'center')
            .in(filePath)
            .in(bgPath)
          resolve()
        })
      })
    } else if (mode === 2) { // 填充
      let bgPath = path.join(outputPath, `._bg_${img.name}`)
      let tempPath = path.join(outputPath, `._temp_${img.name}`)

      if (width > height) {
        console.log('mode2 width > height')

        await new Promise((resolve, reject) => {
          gm(targetWidth, targetHeight, fillColor).write(bgPath, (err) => {
            if (err) return null

            imgObj.resize(targetWidth, null).write(tempPath, (err) => {
              if (err) return null

              imgObj = gm()
                .command('composite')
                .in('-gravity', 'center')
                .in(tempPath)
                .in(bgPath)
              resolve()
            })
          })
        })
      } else {
        console.log('mode2 height > width')

        await new Promise((resolve, reject) => {
          gm(targetWidth, targetHeight, fillColor).write(bgPath, (err) => {
            if (err) return null

            imgObj.resize(null, targetHeight).write(tempPath, (err) => {
              if (err) return null

              imgObj = gm()
                .command('composite')
                .in('-gravity', 'center')
                .in(tempPath)
                .in(bgPath)
              resolve()
            })
          })
        })
      }
    } else if (mode === 1) { // 裁切
      if (width > height) {
        console.log('mode1 width > height')
        imgObj = imgObj.resize(null, targetHeight).crop(targetWidth, targetHeight, Math.round((calc(height, width) - targetHeight) / 2), 0)
      } else {
        console.log('mode1 height > width')
        imgObj = imgObj.resize(targetWidth, null).crop(targetWidth, targetHeight, 0, Math.round((calc(width, height) - targetWidth) / 2))
      }
    }

    let destDirectory = img.getDestDirectory(outputPath)
    if (!fs.existsSync(destDirectory)) mkDirByPathSync(destDirectory, { recursive: false })

    imgObj.write(img.getDestPath(outputPath), err => {
      if (err) {
        console.error(`解析图片【${img.name}】出现错误：`, JSON.stringify(err))
        fail(`解析图片【${img.name}】出现错误：${JSON.stringify(err)}`)
        if (images.length > 0) return check()
      }

      console.log(`解析图片【${img.name}】完成`)
      console.log('%c┗ ----------------------------', 'background-color:yellow')
      console.log('')

      if (images.length > 0) return check()

      if (images.length === 0) {
        fs.readdirSync(outputPath).map((x) => {
          if (x.startsWith('._')) fs.unlinkSync(path.join(outputPath, x))
        })

        return success()
      }
    })
  })()
}
