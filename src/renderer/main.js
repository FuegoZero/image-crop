import 'element-ui/lib/theme-chalk/index.css'

import Vue from 'vue'
import axios from 'axios'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'

import './style/init.scss'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.use(ElementUI, { size: 'medium', zIndex: 800 })
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  template: '<App/>'
}).$mount('#app')
